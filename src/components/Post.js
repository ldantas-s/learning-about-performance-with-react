import React from "react";

const Post = ({ post, onAddToWishlist }) => {
	return (
		<li>
			<strong>{post.title}</strong>
			<p>{post.body}</p>
			<button onClick={() => onAddToWishlist(post.title)}>
				add to wishlist
			</button>
		</li>
	);
};

export default React.memo(Post);

/**
 * export default React.memo(Post, (prevProps, nextProps) => {
 *  if (rerender) return fasle
 *  if (notRerender) return true
 * });
 */
