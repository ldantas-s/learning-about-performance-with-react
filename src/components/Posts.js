import React, { useEffect, useState, useCallback, useMemo } from "react";
import Post from "./Post";

const Posts = () => {
	const [posts, setPosts] = useState([]);
	const [newPost, setNewPost] = useState("");
	const [wishlist, setWishlist] = useState([]);
	const lengthAboutLetterX = useMemo(() => {
		console.log("recalculo");
		return posts?.filter(({ title }) => title.includes("x")).length;
	}, [posts]);

	useEffect(() => {
		fetch("https://jsonplaceholder.typicode.com/posts")
			.then((response) => response.json())
			.then(setPosts);
	}, []);

	const addToWishlist = useCallback(
		(post) => setWishlist((prevState) => [...prevState, post]),
		[]
	);

	return (
		<>
			<input
				type="text"
				onChange={(e) => setNewPost(e.target.value)}
				value={newPost}
			/>
			<p>Count about letter X: {lengthAboutLetterX}</p>
			<ol>
				{posts.map((post) => (
					<Post {...{ key: post.id, post, onAddToWishlist: addToWishlist }} />
				))}
			</ol>
		</>
	);
};

export default Posts;
