import React from 'react';

import Title from './Title';
import Count from './Count';

const Article = ({ title, number }) => {
  return (
    <>
      <Title {...{ title }} />
      <Count {...{ number }} />
    </>
  );
};

export default Article;
